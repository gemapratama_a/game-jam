extends Button

export (String) var sceneToLoad

func _on_BackToMenu_pressed():
	get_tree().change_scene(str("res://scenes/" + sceneToLoad + ".tscn"))
