extends Area2D

export (int) var speed
export (int) var damage
export (float) var lifetime

var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func initialize(_position, _direction):
	position = _position
	rotation = _direction.angle()
	$Timer.wait_time = lifetime
	velocity = _direction * speed
	
func _process(delta):
	position += velocity * delta

func _on_Bullet_body_entered(body):
	explode()
	#print("KENA" + str(body))
	if body.has_method("setHp"):
		body.setHp(body.getHp() - damage)
		$BulletHit.play()
		print(str(body) +"'s HP: "+ str(body.getHp()))
	
func explode():
	$BulletHit.play()
	queue_free()

# https://www.youtube.com/watch?v=UKfzBnfh4Ak