# Zombrun

Zombrun is a top-down shooting game where the player's objective is to exhaust the zombie counter in each level. The player is only given one life.

Gema Pratama Aditya - 1706040031

Controls:
WASD or Arrow keys: Movement
Left click: Shoot and Kill!

Objective:
Clear two levels by killing all the zombies!