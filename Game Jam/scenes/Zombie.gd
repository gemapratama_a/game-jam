extends "res://scripts/Character.gd"

signal regen

onready var parent = get_parent().get_parent()
onready var player = get_node("/root/Level1/Player")
export (float) var aggroSpeed
export (int) var visionRadius
export (float) var hpRegenTimer

var zombie = self
var zombieSpeed = 20
var velocty = 0
var target

var canRegen = true

func _ready():
	#print("PLAYER:" + str(player))
	$HpRegenTimer.wait_time = hpRegenTimer
	$VisionRadius/CollisionShape2D.shape.radius = 500
	#print("HPREGENTIMER:"+str(hpRegenTimer))
	#print("VISION RADIUS:")
	#print($VisionRadius/CollisionShape2D.shape.radius)

"""
func control(delta):
	#print(parent)
	if parent is PathFollow2D:
		#print("MASUK TRUE")
		parent.set_offset(parent.get_offset() + speed * delta)
		position = Vector2()
"""
func _physics_process(delta):
	if target != null:
		#print("MASUK TARGET GA NULL")
		var direction = (target.global_position - global_position).normalized()
		var test = target.get_position_in_parent()
		var tarDir = direction.angle_to(target.position - self.position)#.normalized()
		var d = Vector2(cos(tarDir), sin(tarDir))
		rotation = -d.angle() - 0.3 # <-- MINESIN D ANGLENYA
		move_and_collide(direction * zombieSpeed * delta)

func checkHp():
	pass

func _on_VisionRadius_body_entered(body):
	target = body

func _on_VisionRadius_body_exited(body):
	#print("MASUK TARGET NULL")
	target = null

func _on_HpRegenTimer_timeout():
	regenerateHp()
	#print("REGEN! NEW HP: " + str(self.hp))

func regenerateHp():
	#print("CAN REGEN? " + str(canRegenHp))
	if canRegen:
		emit_signal('regen')
		if self.hp < self.maxHp:
			canRegen = false
			var hpRegenTimerPath = str(self.get_path()) + "/HpRegenTimer"
			var hpRegenTimer = get_node(hpRegenTimerPath)
			hpRegenTimer.start()
			emit_signal("regen")
			self.hp += self.hpRegen
			if self.hp > self.maxHp:
				self.hp = self.maxHp

func _on_DamagePlayerRadius_body_entered(body):
	$AttackHit.play()
	body.hp -= self.damage
	#print(str(body) + " IS HIT FOR " + str(self.damage))
#https://godotengine.org/qa/40507/solved-enemy-follow-player
#https://www.reddit.com/r/godot/comments/avedug/trying_to_get_enemy_to_chase_player/