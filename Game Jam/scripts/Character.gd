extends KinematicBody2D

signal attack
signal hpChanged
signal dead

export (PackedScene) var Bullet
export (int) var speed
export (float) var hpRegen
export (int) var maxHp
export (int) var hp
export (float) var weaponCooldown
export (float) var reloadSpeed
export (float) var rotationSpeed
export (int) var damage

var velocity = Vector2()
var canAttack = true
var isDead = false
var canSoundGunshot = true

func _ready():
	#print(self.get_path())
	#print(str(self.get_parent()))
	var weaponTimerPath = str(self.get_path()) + "/WeaponTimer"
	var weaponTimer = get_node(weaponTimerPath)
	weaponTimer.wait_time = weaponCooldown
	#print(weaponTimer.wait_time)
	#print("WEAPON TIMER: " + str(weaponTimer))

func control(delta):
	pass

func setHp(newHp):
	self.hp = newHp
	
func getHp():
	return self.hp

func getIsDead():
	return isDead

func disappear():
	global.zombieCounter -= 1
	queue_free()
	
func attack():
	#print("CAN ATTACK?: " + str(canAttack))
	if canAttack:
		#print("SHOT!")
		canAttack = false
		var weaponTimerPath = str(self.get_path()) + "/WeaponTimer"
		var weaponTimer = get_node(weaponTimerPath)
		weaponTimer.start()
		var muzzlePath = str(self.get_path()) + "/Body/Muzzle"
		var muzzle = get_node(muzzlePath)
		var attackDirection = Vector2(1,0).rotated(self.global_rotation) #?? 
		emit_signal('attack', Bullet, muzzle.global_position, attackDirection)

	if canSoundGunshot:
		var weaponTimerPath = str(self.get_path()) + "/WeaponTimer"
		var weaponTimer = get_node(weaponTimerPath)
		var gunshotSoundPath = str(self.get_path()) + "/Gunshot"
		var gunshotSound = get_node(gunshotSoundPath)
		canSoundGunshot = false
		gunshotSound.play()
		weaponTimer.start()
		#weaponTimer.wait_time = weaponCooldown
		#canAttack = true
	
func _physics_process(delta):
	if self.hp <= 0:
		#print(str(self) + " IS DEAD")
		isDead = true
		disappear()
		if not self.get('zombie'):
			#print("TRUE")
			get_tree().change_scene(str("res://scenes/GameOver.tscn"))
	else:
		#rotation +=  
		control(delta)
		move_and_collide(velocity)


func _on_WeaponTimer_timeout():
	#$Gunshot.play()
	canAttack = true
	canSoundGunshot = true

"""
func _on_HpRegenTimer_timeout():
	hp += hpRegen
	print("REGEN! NEW HP: " + str(hp))
"""