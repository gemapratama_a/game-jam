extends "res://scripts/Character.gd"

func control(delta):

	var mousePosition = get_local_mouse_position()
	rotation += mousePosition.angle()
	var rotationDirection = 0
	var moveLeft = Input.is_action_pressed('ui_left')
	var moveRight = Input.is_action_pressed('ui_right')
	var moveUp = Input.is_action_pressed('ui_up')
	var moveDown = Input.is_action_pressed('ui_down')
	var shoot = Input.is_action_pressed('shoot')
	
	if moveLeft:
		velocity = Vector2(-speed, 0)#.rotated(rotation)
	if moveRight:
		velocity = Vector2(speed, 0)#.rotated(rotation)
	if moveUp:
		velocity = Vector2(0, -speed)#.rotated(rotation
	if moveDown:
		velocity = Vector2(0, speed)#.rotated(rotation)
	if moveUp and moveLeft:
		velocity = Vector2(-speed, -speed)
	if moveUp and moveRight:
		velocity = Vector2(speed, -speed)
	if moveDown and moveLeft:
		velocity = Vector2(-speed, speed)
	if moveDown and moveRight:
		velocity = Vector2(speed, speed)
	if not moveLeft and not moveRight and not moveUp and not moveDown:
		velocity = Vector2(0, 0)
	if shoot:
		#print("SHOOT")
		attack()

	checkHp()
	
func checkHp():
	#print(str(self) + "'S HP: " + str(self.hp))
	if self.hp <= 0:
		get_tree().change_scene(str("res://scenes/GameOver.tscn"))
