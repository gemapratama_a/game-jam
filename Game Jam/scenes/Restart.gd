extends Button

export (String) var sceneToLoad

func _on_Restart_pressed():
	get_tree().change_scene(str("res://levels/" + sceneToLoad + ".tscn"))