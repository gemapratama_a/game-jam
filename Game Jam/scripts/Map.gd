extends Node

export (int) var zombieRemaining

func _ready():
	global.zombieCounter = 11
	setCameraLimits()
	$BGM.play()

func _process(delta):
	#print(global.zombieCounter)
	if global.zombieCounter == 0:
		get_tree().change_scene(str("res://levels/Level2.tscn"))
	
func setCameraLimits():
	var groundPath = str(self.get_path()) + "/LevelGround"
	var ground = get_node(groundPath)
	var playerPath = str(self.get_path()) + "/Player"
	var player = get_node(playerPath)
	
	var cameraPath = "/root/Level1/Player/Player/Camera2D" #????
	var camera = get_node(cameraPath)
	#print(camera)
	var mapLimits = ground.get_used_rect()
	var mapCellSize = ground.cell_size
	
	$Player/Player/Camera2D.limit_left = mapLimits.position.x * mapCellSize.x
	$Player/Player/Camera2D.limit_right = mapLimits.end.x * mapCellSize.x
	$Player/Player/Camera2D.limit_top= mapLimits.position.y * mapCellSize.y
	$Player/Player/Camera2D.limit_bottom = mapLimits.end.y * mapCellSize.y
	
func _on_Character_Attack(bullet, _position, _direction):
	#print("MASUK CHARACTER ATTACK")
	var b = bullet.instance()
	#print("BULLET: " + str(b))
	add_child(b)
	b.initialize(_position, _direction)

func _on_WeaponTimer_timeout():
	pass