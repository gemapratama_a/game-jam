extends Node2D

var tileSize = Vector2(128, 128)
onready var texture = get_node("Sprite").texture

func _ready():
	print(texture)
	var textureWidth = texture.get_width() / tileSize.x
	var textureHeight = texture.get_height() / tileSize.y
	var newTileSet = TileSet.new()
	for x in range(textureWidth):
		for y in range(textureHeight):
			var region = Rect2(x * tileSize.x, y * tileSize.y,
							tileSize.x, tileSize.y)
			var id = x + y*10
			newTileSet.create_tile(id)
			newTileSet.tile_set_texture(id, texture)
			newTileSet.tile_set_region(id, region)
	ResourceSaver.save("res://terrain/terrainTiles.tres", newTileSet)